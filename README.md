## My Awesome Linux

Repository for fun and awesome linux customization ;) :D

These customizations are tested under Ubuntu 16.04. However most of the customizations are available across
distros, feel free to try them.

## Conky
Conky lets us render high level widget toolkits. We can customize the widgets and let it render on the desktop.

```
sudo apt install conky
```

## Update the Plymouth theme
    sudo update-alternatives --config default.plymouth
    sudo update-initramfs -u
Select the plymouth theme desired when prompted